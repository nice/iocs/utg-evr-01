# -----------------------------------------------------------------------------
# Detector Readout DEV-01
# -----------------------------------------------------------------------------
require mrfioc2
require evr_seq_calc
require cntpstats

# -----------------------------------------------------------------------------
# Environment settings
# -----------------------------------------------------------------------------

epicsEnvSet("TOP", "$(E3_CMD_TOP)/..")

epicsEnvSet("IOC",          "Utg-Ymir:sc-IOC-01")
epicsEnvSet("EVR",          "EVR-01")
epicsEnvSet("SYS",          "Utg-Ymir:")
epicsEnvSet("DEV",          "TS-$(EVR)")
epicsEnvSet("SYSPV",        "$(SYS)$(DEV)")
epicsEnvSet("MRF_HW_DB",    "evr-pcie-300dc-univ.db")
epicsEnvSet("PCIID",        "1:0.0")
epicsEnvSet("CHOP_SYS",     "$(SYS)")
epicsEnvSet("CHOP_DRV1",    "Chop-Drv-01")
epicsEnvSet("CHOP_DRV2",    "Chop-Drv-na02")
epicsEnvSet("CHOP_DRV3",    "Chop-Drv-na03")
epicsEnvSet("CHOP_DRV4",    "Chop-Drv-na04")
epicsEnvSet("BASEEVTNO",    "26")

# -----------------------------------------------------------------------------
# e3 Common databases, autosave, etc.
# -----------------------------------------------------------------------------
iocshLoad("$(essioc_DIR)/essioc.iocsh")

# EVR
iocshLoad("$(mrfioc2_DIR)/evr.iocsh", "EVRDB=$(MRF_HW_DB), P=$(SYSPV), OBJ=$(EVR), PCIID=$(PCIID)")

iocshLoad("$(mrfioc2_DIR)/evrevt.iocsh", "P=$(SYSPV), OBJ=$(EVR)")

dbLoadRecords("mrmevrtsbuf-univ.db","P=$(SYSPV), R="", S=":00-", EVR=$(EVR), CODE=21, TRIG=14, FLUSH=TimesRelPrevFlush, NELM=1000")
dbLoadRecords("mrmevrtsbuf-univ.db","P=$(SYSPV), R="", S=":01-", EVR=$(EVR), CODE=22, TRIG=14, FLUSH=TimesRelPrevFlush, NELM=1000")
dbLoadRecords("mrmevrtsbuf-univ.db","P=$(SYSPV), R="", S=":02-", EVR=$(EVR), CODE=23, TRIG=14, FLUSH=TimesRelPrevFlush, NELM=1000")
dbLoadRecords("mrmevrtsbuf-univ.db","P=$(SYSPV), R="", S=":03-", EVR=$(EVR), CODE=24, TRIG=14, FLUSH=TimesRelPrevFlush, NELM=1000")

# Make the EVR the time sources for the machine
time2ntp("$(EVR)", 2)

# Load the sequencer configuration script
iocshLoad("$(evr_seq_calc_DIR)/evr_seq_calc.iocsh", "P=$(CHOP_SYS), R1=$(CHOP_DRV1), R2=$(CHOP_DRV2), R3=$(CHOP_DRV3), R4=$(CHOP_DRV4), EVR=$(DEV), BASEEVTNO=$(BASEEVTNO)")

iocshLoad("$(cntpstats_DIR)/cntpstats.iocsh","SYS=$(SYS), DEV=$(DEV)")

iocInit()

iocshLoad("$(mrfioc2_DIR)/evr.r.iocsh", "P=$(SYSPV), OBJ=$(EVR)")

#fpgaevr
dbpf $(SYSPV):DlyGen-1-Evt-Trig0-SP 140
dbpf $(SYSPV):DlyGen-1-Width-SP 100
dbpf $(SYSPV):Out-RB06-Src-Pulse-SP "Pulser 1"
dbpf $(SYSPV):EvtH-SP.OUT "@OBJ=$(EVR),Code=140"

##### mini chopper i/o ######

dbpf $(SYSPV):EvtA-SP.OUT "@OBJ=$(EVR),Code=21" 
dbpf $(SYSPV):EvtA-SP.VAL 21 
dbpf $(SYSPV):EvtB-SP.OUT "@OBJ=$(EVR),Code=22" 
dbpf $(SYSPV):EvtB-SP.VAL 22 
dbpf $(SYSPV):EvtC-SP.OUT "@OBJ=$(EVR),Code=23" 
dbpf $(SYSPV):EvtC-SP.VAL 23
dbpf $(SYSPV):EvtD-SP.OUT "@OBJ=$(EVR),Code=24"
dbpf $(SYSPV):EvtD-SP.VAL 24
dbpf $(SYSPV):EvtE-SP.OUT "@OBJ=$(EVR),Code=14"
dbpf $(SYSPV):EvtE-SP.VAL 14
dbpf $(SYSPV):EvtF-SP.OUT "@OBJ=$(EVR),Code=140"
dbpf $(SYSPV):EvtF-SP.VAL 15
dbpf $(SYSPV):EvtG-SP.OUT "@OBJ=$(EVR),Code=125"
dbpf $(SYSPV):EvtG-SP.VAL 16
dbpf $(SYSPV):EvtH-SP.OUT "@OBJ=$(EVR),Code=26"
dbpf $(SYSPV):EvtH-SP.VAL 26



# Trig-Ext-Sel changed from "Off" to "Edge", Code-Ext-SP changed from 0 to 10
# Trig-Ext-Sel changed from "Off" to "Edge", Code-Ext-SP changed from 0 to 10
dbpf $(SYSPV):UnivIn-0-Lvl-Sel "Active High"
dbpf $(SYSPV):UnivIn-0-Edge-Sel "Active Falling"
dbpf $(SYSPV):Out-RB00-Src-SP 61
dbpf $(SYSPV):UnivIn-0-Trig-Ext-Sel "Edge"
dbpf $(SYSPV):UnivIn-0-Code-Ext-SP 21

dbpf $(SYSPV):UnivIn-1-Lvl-Sel "Active High"
dbpf $(SYSPV):UnivIn-1-Edge-Sel "Active Rising"
dbpf $(SYSPV):Out-RB01-Src-SP 61
dbpf $(SYSPV):UnivIn-1-Trig-Ext-Sel "Edge"
dbpf $(SYSPV):UnivIn-1-Code-Ext-SP 22

dbpf $(SYSPV):UnivIn-2-Lvl-Sel "Active High"
dbpf $(SYSPV):UnivIn-2-Edge-Sel "Active Rising"
dbpf $(SYSPV):Out-RB02-Src-SP 61
dbpf $(SYSPV):UnivIn-2-Trig-Ext-Sel "Edge"
dbpf $(SYSPV):UnivIn-2-Code-Ext-SP 23

dbpf $(SYSPV):UnivIn-3-Lvl-Sel "Active Low"
dbpf $(SYSPV):UnivIn-3-Edge-Sel "Active Falling"
dbpf $(SYSPV):Out-RB03-Src-SP 61
dbpf $(SYSPV):UnivIn-3-Trig-Ext-Sel "Edge"
dbpf $(SYSPV):UnivIn-3-Code-Ext-SP 24





#Set up delay generator 0 to trigger on event 14
dbpf $(SYSPV):DlyGen-0-Width-SP 2860 
dbpf $(SYSPV):DlyGen-0-Delay-SP 0 #0ms
dbpf $(SYSPV):DlyGen-0-Evt-Trig0-SP 14

#Set up delay generator 2 to trigger on event 17
dbpf $(SYSPV):DlyGen-2-Width-SP 1000 #1ms
dbpf $(SYSPV):DlyGen-2-Delay-SP 0 #0ms
dbpf $(SYSPV):DlyGen-2-Evt-Trig0-SP 26
dbpf $(SYSPV):Out-RB04-Src-SP 2

#Set up delay generator 3 to trigger on event 125
dbpf $(SYSPV):DlyGen-3-Width-SP 1000 #1ms
dbpf $(SYSPV):DlyGen-3-Delay-SP 100000 #0ms
dbpf $(SYSPV):DlyGen-3-Evt-Trig0-SP 125
dbpf $(SYSPV):Out-RB05-Src-SP 3


######## Sequencer #########
dbpf $(SYSPV):End-Event-Ticks 4
# Load sequencer setup
dbpf $(SYSPV):SoftSeq-0-Load-Cmd 1
# Enable sequencer
dbpf $(SYSPV):SoftSeq-0-Enable-Cmd 1
# Select run mode, "Single" needs a new Enable-Cmd every time, "Normal" needs Enable-Cmd once
dbpf $(SYSPV):SoftSeq-0-RunMode-Sel "Normal"
# Use ticks or microseconds
dbpf $(SYSPV):SoftSeq-0-TsResolution-Sel "Ticks"
# Select trigger source for soft seq 0, trigger source 0, delay gen 0
dbpf $(SYSPV):SoftSeq-0-TrigSrc-0-Sel 0
# Commit all the settings for the sequnce
# commit-cmd by evrseq!!! 
epicsThreadSleep 2
dbpf $(SYSPV):SoftSeq-0-Commit-Cmd "1"

